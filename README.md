# WiFiAce
>WiFi ACE is a simplified web interface that allows the security analyst to audit the wireless networks stealthy using raspberry pi.

Apart from raspberry pi WifiAce works on any linux based operating system that meets the below requirements.
![Dashboard](https://2.bp.blogspot.com/-BSsbeKiQobg/WrKiaDWvMcI/AAAAAAAAIjw/BAIbVIF2Bo8NDbSOGj5OJ5e2as5aHLG7wCLcBGAs/s640/Screenshot%2Bat%2B2018-03-15%2B10-58-02.png)
## Features 
 * **_Scanning_** - Allows scanning of the vicinity and gather information about the devices.
 * **_Rouge Access Point_** - With the help of hostapd-mana combined with **Karma** and **MANA** attacks.
 * **_Modular_** - Easy to create plugin to extend the functionality on the go.
 * **_Easy to use_** - No tricky commands to remember simplified and responsive web interface.
 * **_Accessiblity_** - Can be easily accessed from any device in the current network.

#### Requirements
  * aircrack-ng
  * subversion
  * python-apt
  * hostapd-mana

#### Installing wifiace
WiFi can be easily installed on any debian based OS by running the ````setup.py```` script. 
Remember to clone repository using ````--recursive```` option.
For OS other than debian install above equivalent packages. Follow the below steps and build **hostapd-mana**.
```sh
$ git clone --recursive https://github.com
$ cd wifiace/
$ pip install -r requirements.txt 
$ cp config/default/wifiace.conf /etc/
```

#### Hostapd-mana requirements
- libssl1.0-dev  
- bridge-utils 
- libnl-genl-3-dev

### Building hostapd-mana
```sh
$ cd external_tools/hostapd-mana/hostapd
$ make
```

### Launching WifiAce
WiFiAce requires ```root``` privilages for running. Following are the launch options for *wifiace*. 
The default ID and password is ```root:toor```

```
# python wifiace.py -h
usage: wifiace [-h] [-a HOST] [-p PORT] [-D]

This script starts flask server and deploys wifiace-web on it.

optional arguments:
  -h, --help            show this help message and exit
  -a HOST, --host HOST  IP address on which the server should bind leave it
                        blank to bind to localhost
  -p PORT, --port PORT  port for the webserver to run on (Default : 5000)
  -D, --debug           Starts wifiace in debug mode.(Warning:Instace checking
                        doesn't work in this mode.)

```
If you want to know more about *wifiace* please refer the [wiki](https://bitbucket.org/a5hish/wifiace/wiki).